With the improved Docker setup, you will get:

Nginx
PHP 7.4
MySQL 8
PHPMyAdmin
Composer commands: access composer commands directly without entering the container
Artisan commands: access artisan commands directly without entering the container
In a nutshell, you only need to install Docker as well as docker-compose on your (host) machine and let Docker does the rest

Step to install:
Run cp .env.example .env
If you think the default setting are pretty well setup, you can run ./docker-install.sh right away from your terminal.
Otherwise, read a section below for further information about properties you may change.
Run ./docker-install.sh
Note
Here are .env properties you can change:

Common Laravel .env properties
DOCKER_WEBSERVER_HOST defines the Unifiedtransform port address. default value: 4049
DOCKER_PHPMYADMIN_HOST defines the PHPMyAdmin port address. default value: 5051
You can also customize the database name defined on the DB_DATABASE property when Docker is being initialized the first time.

Added Commands
docker-compose run --rm composer <commands>
docker-compose run --rm artisan <commands>
Not using a Container:
Here are some basic steps to start using this application

Note: Instruction on cached data for Dashboard is given in Good to know segment below.

Clone the repository
git clone https://github.com/changeweb/Unifiedtransform
Copy the contents of the .env.example file to create .env in the same directory

Run composer install for developer environment and run composer install --optimize-autoloader --no-dev for production environment to install Laravel packages (Remove Laravel Debugbar, Laravel Log viewer packages from composer.json and

   //Provider
   Barryvdh\Debugbar\ServiceProvider,
   Logviewer Service provider,
   //Alias
   'Debugbar' => Barryvdh...
from config/app.php before running composer install in Production Environment)

Generate APP_KEY using php artisan key:generate

Edit the database connection configuration in .env file e.g.

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=unifiedtransform
DB_USERNAME=unified
DB_PASSWORD=secret
Note that this is just an example, and the values may vary depending on your database environment.

Set the APP_ENV variable in your .env file according to your application environment (e.g. local, production) in .env file

Migrate your Database with php artisan migrate

Seed your Database with php artisan db:seed

On localhost, serve your application with php artisan serve